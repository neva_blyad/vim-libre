"    vim-libre.vim
"    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
"                                            <neva_blyad@lovecri.es>
"
"    This program is free software: you can redistribute it and/or modify
"    it under the terms of the GNU General Public License as published by
"    the Free Software Foundation, either version 3 of the License, or
"    (at your option) any later version.
"
"    This program is distributed in the hope that it will be useful,
"    but WITHOUT ANY WARRANTY; without even the implied warranty of
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"    GNU General Public License for more details.
"
"    You should have received a copy of the GNU General Public License
"    along with this program.  If not, see <https://www.gnu.org/licenses/>.

highlight clear

set background=dark

if exists("syntax_on")
    syntax reset
endif

let g:colors_name = "vim-libre"

highlight Normal          term=NONE         cterm=NONE           ctermfg=231 ctermbg=16   gui=NONE           guifg=#FFFFFF guibg=#000000
highlight Scrollbar       term=NONE         cterm=NONE                                    gui=NONE
highlight Menu            term=NONE         cterm=NONE                                    gui=NONE
highlight SpecialKey      term=bold         cterm=bold           ctermfg=196              gui=bold           guifg=#FF0000
highlight NonText         term=bold         cterm=bold           ctermfg=196              gui=bold           guifg=#FF0000
highlight Directory       term=bold         cterm=bold           ctermfg=56               gui=bold           guifg=#5F00DF
highlight ErrorMsg        term=standout     cterm=bold           ctermfg=231 ctermbg=196  gui=bold           guifg=#FFFFFF guibg=#FF0000
highlight Search          term=reverse      cterm=bold           ctermfg=22  ctermbg=154  gui=bold           guifg=#005F00 guibg=#AFFF00
highlight MoreMsg         term=bold         cterm=bold           ctermfg=22               gui=bold           guifg=#005F00
highlight ModeMsg         term=bold         cterm=bold                                    gui=bold
highlight LineNr          term=underline    cterm=bold           ctermfg=240 ctermbg=NONE gui=bold           guifg=#585858 guibg=#000000
highlight CursorLineNr    term=underline    cterm=bold           ctermfg=250 ctermbg=235  gui=bold           guifg=#BCBCBC guibg=#262626
highlight Question        term=standout     cterm=bold           ctermfg=250 ctermbg=240  gui=bold           guifg=#BCBCBC guibg=#585858
highlight StatusLine      term=bold,reverse cterm=bold           ctermfg=22  ctermbg=154  gui=bold           guifg=#005F00 guibg=#AFFF00
highlight StatusLineNC    term=reverse      cterm=NONE           ctermfg=250 ctermbg=237  gui=NONE           guifg=#FFFFFF guibg=#585858
highlight Title           term=bold         cterm=bold           ctermfg=163              gui=bold           guifg=#DF00AF
highlight Visual          term=reverse      cterm=reverse                                 gui=reverse
highlight WarningMsg      term=standout     cterm=bold           ctermfg=196              gui=bold           guifg=#FF0000
highlight Cursor          term=NONE         cterm=NONE                                    gui=NONE
highlight Comment         term=bold         cterm=bold           ctermfg=240              gui=bold           guifg=#585858
highlight Constant        term=underline    cterm=bold           ctermfg=199              gui=bold           guifg=#FF00AF
highlight Special         term=bold         cterm=bold           ctermfg=196              gui=bold           guifg=#FF0000
highlight Identifier      term=underline    cterm=NONE           ctermfg=56               gui=NONE           guifg=#5F00DF
highlight Statement       term=bold         cterm=bold           ctermfg=154              gui=bold           guifg=#AFFF00
highlight PreProc         term=underline    cterm=NONE           ctermfg=163              gui=NONE           guifg=#DF00AF
highlight Type            term=underline    cterm=bold           ctermfg=154              gui=bold           guifg=#AFFF00
highlight Error           term=reverse      cterm=NONE           ctermfg=240 ctermbg=16   gui=NONE           guifg=#585858 guibg=#000000
highlight Todo            term=standout     cterm=NONE           ctermfg=250 ctermbg=240  gui=NONE           guifg=#BCBCBC guibg=#585858
highlight CursorLine      term=NONE         cterm=NONE                       ctermbg=235  gui=NONE                         guibg=#262626
highlight CursorColumn    term=NONE         cterm=NONE                       ctermbg=235  gui=NONE                         guibg=#262626
highlight MatchParen      term=reverse      cterm=NONE           ctermfg=22  ctermbg=154  gui=NONE           guifg=#005F00 guibg=#AFFF00
highlight TabLine         term=bold,reverse cterm=bold           ctermfg=240 ctermbg=231  gui=bold           guifg=#585858 guibg=#FFFFFF
highlight TabLineFill     term=bold,reverse cterm=bold           ctermfg=240 ctermbg=231  gui=bold           guifg=#585858 guibg=#FFFFFF
highlight TabLineSel      term=reverse      cterm=NONE           ctermfg=231 ctermbg=240  gui=NONE           guifg=#FFFFFF guibg=#585858
highlight Underlined      term=underline    cterm=bold,underline ctermfg=240              gui=bold,underline guifg=#585858
highlight Ignore          term=NONE         cterm=NONE           ctermfg=16  ctermbg=16   gui=NONE           guifg=#000000 guibg=#000000
highlight EndOfBuffer     term=bold         cterm=bold           ctermfg=196              gui=bold           guifg=#FF0000
highlight VertSplit       term=bold         cterm=bold           ctermfg=250 ctermbg=237  gui=bold           guifg=#BCBCBC guibg=#3A3A3A
highlight Folded          term=NONE         cterm=NONE           ctermfg=250 ctermbg=235  gui=NONE           guifg=#BCBCBC guibg=#262626

highlight debugPC         term=reverse      cterm=NONE                       ctermbg=235  gui=NONE                         guibg=#262626
highlight debugBreakpoint term=reverse      cterm=NONE                       ctermbg=196  gui=NONE                         guibg=#FF0000

highlight link IncSearch      Visual
highlight link String         Constant
highlight link Character      Constant
highlight link Number         Constant
highlight link Boolean        Constant
highlight link Float          Number
highlight link Function       Identifier
highlight link Conditional    Statement
highlight link Repeat         Statement
highlight link Label          Statement
highlight link Operator       Statement
highlight link Keyword        Statement
highlight link Exception      Statement
highlight link Include        PreProc
highlight link Define         PreProc
highlight link Macro          PreProc
highlight link PreCondit      PreProc
highlight link StorageClass   Type
highlight link Structure      Type
highlight link Typedef        Type
highlight link Tag            Special
highlight link SpecialChar    Special
highlight link Delimiter      Special
highlight link SpecialComment Special
highlight link Debug          Special
